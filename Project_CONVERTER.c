#include <stdio.h>
struct convertor
{
    int firstUnitChoice, secondUnitChoice;
    float firstInput;
    float conversion;
};


struct convertor lengthConversion(int firstUnit,int secondUnit,float first )
{
    struct convertor b;
    if( firstUnit == 1 && secondUnit == 1 )
    {
        b.conversion = first;
    }
    else if( firstUnit == 1 && secondUnit == 2 )
    {
        b.conversion = first / 10;
    }
    else if( firstUnit == 1 && secondUnit == 3 )
    {
        b.conversion = first / 1000;
    }
    else if( firstUnit == 1 && secondUnit == 4 )
    {
        b.conversion = first / 1000000;
    }
    else if( firstUnit == 2 && secondUnit == 1 )
    {
        b.conversion = first * 10;
    }
    else if( firstUnit == 2 && secondUnit == 2 )
    {
        b.conversion = first;
    }
    else if( firstUnit == 2 && secondUnit == 3 )
    {
        b.conversion = first / 100;
    }
    else if( firstUnit == 2 && secondUnit == 4 )
    {
        b.conversion = first * .00001;
    }
    else if( firstUnit == 3 && secondUnit == 1 )
    {
        b.conversion = first * 1000;
    }
    else if( firstUnit == 3 && secondUnit == 2 )
    {
        b.conversion = first * 100;
    }
    else if( firstUnit == 3 && secondUnit == 3 )
    {
        b.conversion = first;
    }
    else if( firstUnit == 3 && secondUnit == 4 )
    {
        b.conversion = first / 1000;
    }
    else if( firstUnit == 4 && secondUnit == 1 )
    {
        b.conversion = first * 1000000;
    }
    else if( firstUnit == 4 && secondUnit == 2 )
    {
        b.conversion = first * 100000;
    }
    else if( firstUnit == 4 && secondUnit == 3 )
    {
        b.conversion = first * 1000;
    }
    else if( firstUnit == 4 && secondUnit == 4 )
    {
        b.conversion = first;
    }
    return b;
}
struct convertor weightConversion( int firstUnit, int secondUnit,float first )
{
    struct convertor b;
    if( firstUnit == 1 && secondUnit == 1 )
    {
        b.conversion = first;
    }
    else if( firstUnit == 1 && secondUnit == 2 )  // mg to centigram
    {
        b.conversion =  first/455360;
    }
    else if( firstUnit == 1 && secondUnit == 3 )  // mg to g
    {
        b.conversion = first / 1000;
    }
    else if( firstUnit == 1 && secondUnit == 4 )  // mg to kg
    {
        b.conversion = first * .000001;
    }
    else if( firstUnit == 2 && secondUnit == 1 )  // centigram to mg
    {
        b.conversion = first * 10;
    }
    else if( firstUnit == 2 && secondUnit == 2 )  // centigram to centigram
    {
        b.conversion = first;
    }
    else if( firstUnit == 2 && secondUnit == 3 )  // centigram to gram
    {
        b.conversion = first / 455.36;
    }
    else if( firstUnit == 2 && secondUnit == 4 )  // centigram to kilogram
    {
        b.conversion = first * .45536;
    }
    else if( firstUnit == 3 && secondUnit == 1 )
    {
        b.conversion = first * 1000;
    }
    else if( firstUnit == 3 && secondUnit == 2 )
    {
        b.conversion = first / 455.36;
    }
    else if( firstUnit == 3 && secondUnit == 3 )
    {
        b.conversion = first;
    }
    else if( firstUnit == 3 && secondUnit == 4 )
    {
        b.conversion = first / 1000;
    }
    else if( firstUnit == 4 && secondUnit == 1 )
    {
        b.conversion = first * 1000000;
    }
    else if( firstUnit == 4 && secondUnit == 2 )
    {
        b.conversion = first * 0.45536;
    }
    else if( firstUnit == 4 && secondUnit == 3 )
    {
        b.conversion = first * 1000;
    }
    else if( firstUnit == 4 && secondUnit == 4 )
    {
        b.conversion = first;
    }
    return b;
}
struct convertor volumeConversion( int firstUnit, int secondUnit,float first )
{
   struct convertor b;
    if( firstUnit == 1 && secondUnit == 1 )  // cubic centimeter to cubic centimeter
    {
        b.conversion = first;
    }
    else if( firstUnit == 1 && secondUnit == 2 )  // cubic centimeter to cubic meter
    {
        b.conversion = first * .000001;
    }
    else if( firstUnit == 1 && secondUnit == 3 )  // cubic centimeter to cubic feet
    {
        b.conversion = first / 1000;
    }
    else if( firstUnit == 1 && secondUnit == 4 ) // cubic centimeter to cubic inches
    {
        b.conversion = first * .000035;
    }
    else if( firstUnit == 2 && secondUnit == 1 )  // cubic meter to cubic centimeter
    {
        b.conversion = first * 1000000;
    }
    else if( firstUnit == 2 && secondUnit == 2 )  // cubic meter to cubic meter
    {
        b.conversion = first;
    }
    else if( firstUnit == 2 && secondUnit == 3 )  // cubic meter to cubic feet
    {
        b.conversion = first * 35.32;
    }
    else if( firstUnit == 2 && secondUnit == 4 )  // cubic meter to cubic inches
    {
        b.conversion = first * 61024;
    }
    else if( firstUnit == 3 && secondUnit == 1 )  // cubic feet to cubic centimeter
    {
        b.conversion = first * 28317;
    }
    else if( firstUnit == 3 && secondUnit == 2 )  // cubic feet to cubic meter
    {
        b.conversion = first * .0283;
    }
    else if( firstUnit == 3 && secondUnit == 3 )  // cubic feet to cubic feet
    {
        b.conversion = first;
    }
    else if( firstUnit == 3 && secondUnit == 4 )  // cubic feet to cubic inches
    {
        b.conversion = first / 1728;
    }
    else if( firstUnit == 4 && secondUnit == 1 )  // cubic inches to cubic centimeter
    {
        b.conversion = first * 1000000;
    }
    else if( firstUnit == 4 && secondUnit == 2 )  // cubic inches to cubic meter
    {
        b.conversion = first * 100000;
    }
    else if( firstUnit == 4 && secondUnit == 3 )  // cubic inches to cubic feet
    {
        b.conversion = first * 1000;
    }
    else if( firstUnit == 4 && secondUnit == 4 )  // cubic inches to cubic inches
    {
        b.conversion = first;
    }
    return b;
}
struct convertor areaConversion( int firstUnit, int secondUnit,float first )
{
    struct convertor b;
    if( firstUnit == 1 && secondUnit == 1 )  // square centimeter to square centimeter
    {
        b.conversion = first;
    }
    else if( firstUnit == 1 && secondUnit == 2 )  // square centimeter to square meter
    {
        b.conversion = first * .0001;
    }
    else if( firstUnit == 1 && secondUnit == 3 )  // square centimeter to square feet
    {
        b.conversion = first * .00108;
    }
    else if( firstUnit == 1 && secondUnit == 4 ) // square centimeter to square inches
    {
        b.conversion = first * .155;
    }
    else if( firstUnit == 2 && secondUnit == 1 )  // square meter to square centimeter
    {
        b.conversion = first * 10000;
    }
    else if( firstUnit == 2 && secondUnit == 2 )  // square meter to square meter
    {
        b.conversion = first;
    }
    else if( firstUnit == 2 && secondUnit == 3 )  // square meter to square feet
    {
        b.conversion = first * 10.764;
    }
    else if( firstUnit == 2 && secondUnit == 4 )  // square meter to square inches
    {
        b.conversion = first * 1550;
    }
    else if( firstUnit == 3 && secondUnit == 1 )  // square feet to square centimeter
    {
        b.conversion = first * 929.0304;
    }
    else if( firstUnit == 3 && secondUnit == 2 )  // square feet to square meter
    {
        b.conversion = first * .0929;
    }
    else if( firstUnit == 3 && secondUnit == 3 )  // square feet to square feet
    {
        b.conversion = first;
    }
    else if( firstUnit == 3 && secondUnit == 4 )  // square feet to square inches
    {
        b.conversion = first / 144;
    }
    else if( firstUnit == 4 && secondUnit == 1 )  // square inches to square centimeter
    {
        b.conversion = first * 6.4516;
    }
    else if( firstUnit == 4 && secondUnit == 2 )  // square inches to square meter
    {
        b.conversion = first * .00065;
    }
    else if( firstUnit == 4 && secondUnit == 3 )  // square inches to square feet
    {
        b.conversion = first * .00694;
    }
    else if( firstUnit == 4 && secondUnit == 4 )  // square inches to square inches
    {
        b.conversion = first;
    }
    return b;
}
struct convertor tempConversion( int firstUnit, int secondUnit,float first )
{
    struct convertor b;
    if( firstUnit == 1 && secondUnit == 1 )  // celcius to celcius
    {
        b.conversion = first;
    }
    else if( firstUnit == 1 && secondUnit == 2 )  //  celcius to fahrenheit
    {
        b.conversion = (9*first/5) + 32;
    }
    else if( firstUnit == 1 && secondUnit == 3 )  //  celcius to kelvin
    {
        b.conversion = first + 273;
    }
    else if( firstUnit == 2 && secondUnit == 1 )  // fahrenheit to celcius
    {
        b.conversion = (first - 32) * 5 / 9;
    }
    else if( firstUnit == 2 && secondUnit == 2 )  // fahrenheit to fahrenheit
    {
        b.conversion = first;
    }
    else if( firstUnit == 2 && secondUnit == 3 )  //  fahrenheit to kelvin
    {
        b.conversion = ((first - 32) * 5/9) + 273;
    }
    else if( firstUnit == 3 && secondUnit == 1 )  // kelvin to celcius
    {
        b.conversion = first - 273;
    }
    else if( firstUnit == 3 && secondUnit == 2 )  // kelvin to fahrenheit
    {
        b.conversion = ((first -273) * 9/5) + 32;
    }
    else if( firstUnit == 3 && secondUnit == 3 )  // kelvin to kelvin
    {
        b.conversion = first;
    }
    return b;
}
int main()
{
    struct convertor c,cc;
    int choice, i;
    while(1)//while(1) is an infinite loop.
    {
        printf("1. length\n2. weight\n3. volume\n4. area\n5. temperature\n6. exit\n");
        printf( "enter your choice: " );
        scanf( "%d",&choice );
        switch( choice )
        {
             //length conversion
            case 1: printf("the conversions are below: \n");
            printf("1. mm\n2. cm\n3. m\n4. km\n");
            printf("enter 1st unit: ");
            scanf("%d", &c.firstUnitChoice);
            printf("\n");
            printf("enter 2nd unit: ");
            scanf("%d", &c.secondUnitChoice);
            printf("\n");
            printf("enter first input: ");
            scanf("%f", &c.firstInput);
            cc=lengthConversion( c.firstUnitChoice, c.secondUnitChoice, c.firstInput );
            printf("\n\tthe conversion result %.15f\n\n",cc.conversion);
            break;
            //weight conversion
            case 2: printf("the conversions are below: \n");
            printf("1. mg\n2. pound\n3. g\n4. kg\n");
            printf("enter 1st unit: ");
            scanf("%d", &c.firstUnitChoice);
            printf("\n");
            printf("enter 2nd unit: ");
            scanf("%d", &c.secondUnitChoice);
            printf("\n");
            printf("enter first input: ");
            scanf("%f", &c.firstInput);
            cc= weightConversion(c.firstUnitChoice,c.secondUnitChoice,c.firstInput );
            printf("\n\tthe conversion result %.15f\n\n",cc.conversion);
            break;
            //volume conversion
            case 3: printf("the conversions are below: \n");
            printf("1. cubic centemeter\n2. cubic meter\n3. cubic feet\n4. cubic inches\n");
            printf("enter 1st unit: ");
            scanf("%d", &c.firstUnitChoice);
            printf("\n");
            printf("enter 2nd unit: ");
            scanf("%d", &c.secondUnitChoice);
            printf("\n");
            printf("enter first input: ");
            scanf("%f", &c.firstInput);
            cc= volumeConversion( c.firstUnitChoice, c.secondUnitChoice, c.firstInput );
            printf("\n\tthe conversion result %.15f\n\n", cc.conversion);
            break;
            //area conversion
            case 4: printf("the conversions are below: \n");
            printf("1. square cm\n2. square m\n3. square feet\n4. square inches\n");
            printf("enter 1st unit: ");
            scanf("%d", &c.firstUnitChoice);
            printf("\n");
            printf("enter 2nd unit: ");
            scanf("%d", &c.secondUnitChoice);
            printf("\n");
            printf("\n\tthe conversion result %.15f\n\n",cc.conversion);
            break;
            //temperature conversion
            case 5: printf("the conversions are below: \n");
            printf("1. degree celcius\n2. fahrenheit\n3. kelvin\n");
            printf("enter 1st unit: ");
            scanf("%d", &c.firstUnitChoice);
            printf("\n");
            printf("enter 2nd unit: ");
            scanf("%d", &c.secondUnitChoice);
            printf("\n");
            printf("enter first input: ");
            scanf("%f", &c.firstInput);
            cc= tempConversion( c.firstUnitChoice, c.secondUnitChoice, c.firstInput );
            printf("\n\tthe conversion result %.15f\n\n",cc.conversion);
            break;
            case 6: exit(0);//we used exit (0) before in if-else.
            default: printf("\nenter choice between 1 and 5\n");
        }
    }
}
